'''TODO

shop
dungeon generation
monsters
items

theme songs for encounters with bosses/unique monsters

'''

#global variables
global password
global username
global temp_race

import pickle #used for creating save file data
import time #used for delays
import os.path #used for checking if a file exists
import random #give access to random numbers
import subprocess as shell #gives access to the underlying shell for things like screen clearing
import getpass #for working with the password


################################################
# Gear Classes
############################################### 

class Item():
        """The base class for all items"""
        def __init__(self, name, description, value, owner):
                self.name = name
                self.description = description
                self.value = value
		self.owner = "none"

class battle_axe(Item):
        def __init__(self):
                self.name = "Battle Axe"
                self.description = "A battle axe"
                self.value = 5
                self.damage = rolldice(1,6)

class club(Item):
        def __init__(self):
                self.name = "Club"
                self.description = "A simple wooden club"
                self.value = 0
                self.damage = rolldice(1,4)

class warhammer(Item):
        def __init__(self):
                self.name = "Warhammer"
                self.description = "A solid blunt instrument of destruction"
                self.value = 1
                self.damage = rolldice(1,4)
		self.damage = self.damage + 1

class staff(Item):
        def __init__(self):
                self.name = "Staff"
                self.description = "A sturdy old treelimb, perfect for walking with."
                self.value = 0
                self.damage = d6(1)


class dagger(Item):
        def __init__(self):
                self.name = "Dagger"
                self.description = "A small dagger with some rust. Somewhat more dangerous than a rock."
                self.value = 2
                self.damage = rolldice(1,4)

class shortsword(Item):
        def __init__(self):
                self.name = "Shortsword"
                self.description = "Made of reasonable quality steel, it is ready for combat"
                self.value = 8
                self.damage = d6(1)


################################################ 

#the main player class with associated variables
class player:
	def __init__(self, name):
        	self.name = "none"
        	self.race = "none"
		self_class = "none"
		self.level = 1
		self.alignment = "none"
        	self.str = 0
        	self.dex = 0
        	self.con = 0
        	self.int = 0
        	self.wis = 0
        	self.cha = 0
		self.xp = 0
		self.xpn = 0
        	self.xpbonus = 0
        	self.hp = 0
		self.maxhp = 0
		self.save = 0
		self.gp = 0
		self.head = "none"
		self.armor = "none"
		self.ring1 = "none"
		self.ring2 = "none"
		self.amulet = "none"
		self.weapon = "none"
		self.offhand = "none"  
		self.inventory = {"item0":"none", "item1":"none", "item2":"none", "item3":"none", "item4":"none", "item5":"none", "item6":"none", "item7":"none", "item8":"none", "item9":"none"}
		self.status = {"confused":0, "hallucinating":0, "asleep":0, "charmed":0, "poisoned":0, "hasted":0}
#		self.spells = {}
#		self.spd = {lvl1:0, lvl2:0, lvl3:0, lvl4:0, lvl5:0, lvl6:0, lvl7:0, lvl8:0, lvl9:0} #spells per day
# print released.get("iphone 3G", "none")


shell.call("clear")
print("                                         S&W Roguelike v. 1.0.0")
print("                                              Programming by")
print("                                             Terminal Dragon")
print("                                                  2017\n\n")
print("                                                   o")
print("                                                 _---|         _ _ _ _ _")
print("                                              o   ---|     o   ]-I-I-I-[")
print("                             _ _ _ _ _ _  _---|      | _---|    \ ` ' /")
print("                             ]-I-I-I-I-[   ---|      |  ---|    |.   |")
print("                              \ `   '_/       |     / \    |    | /^\|")
print("                               [*]  __|       ^    / ^ \   ^    | |*||")
print("                               |__   ,|      / \  /    `\ / \   | ===|")
print("                            ___| ___ ,|__   /    /=_=_=_=\   \  |,  _|")
print("                            I_I__I_I__I_I  (====(_________)___|_|____|____")
print("                            \-\--|-|--/-/  |     I  [ ]__I I_I__|____I_I_|")
print("                             |[]      '|   | []  |`__  . [  \-\--|-|--/-/")
print("                             |.   | |' |___|_____I___|___I___|---------|")
print("                            / \| []   .|_|-|_|-|-|_|-|_|-|_|-| []   [] |")
print("                           <===>  |   .|-=-=-=-=-=-=-=-=-=-=-|   |    / \"")
print("                           ] []|`   [] ||.|.|.|.|.|.|.|.|.|.||-      <===>")
print("                           ] []| ` |   |/////////\\\\\\\\\\.||__.  | |[] [")
print("                           <===>     ' ||||| |   |   | ||||.||  []   <===>")
print("                            \T/  | |-- ||||| | O | O | ||||.|| . |'   \T/")
print("                             |      . _||||| |   |   | ||||.|| |     | |")
print("                          ../|' v . | .|||||/____|____\|||| /|. . | . ./")
print("                           |//\............/...........\........../../\\\"")
time.sleep(3)



def professions():
        shell.call("clear")
	print("Class Help\n\n")
	print("Cleric")
	print("HD: D6")
	print("Prime Attribute: Wisdom") 
	print("Weapons: Blunt Melee only")
	print("Armor: All ")
	print("Special Features: +2 vs paralysis and poisons, divine spell casting\n")

	print("Fighter")
	print("HD: D8")
	print("Prime Attribute: Strength ")
	print("Weapons: All")
	print("Armor: All ")
	print("Special Features: Bonus to hit/Damage based on Str score, Bonus to AC based on Dex\n")

	print("Mage")
	print("HD: D4")
	print("Prime Attribute: Intelligence") 
	print("Weapons: Daggers, Darts, and Staves")
	print("Armor: None")
	print("Special Features: +2 vs magic, arcane spell casting\n")

	print("Thief")
	print("HD: D4")
	print("Prime Attribute: Dexterity") 
	print("Weapons: All")
	print("Armor: Leather only, no shields")
	print("Special Features: scaling critical multiplier, +2 vs devices, thief skills\n")

        raw_input("press enter to continue")

def races():
        shell.call("clear")
	print("Race Help:\n\n")
        print("Humans: 10% additional XP\n")
        print("Dwarves: +4 vs magic, +1 to hit in melee combat\n" )
        print("Halflings: +4 vs magic, +1 to hit in ranged combat\n")
        print("Elves: 1 in 6 chance of finding hidden doors on entering room, 4 in 6 chance when active searching\n")
        print("Half-Elves: +5% additional XP, 2 in 6 chance of finding hidden doors when active searching")
	raw_input("press enter to continue")

def tavern_shop():
	shell.call("clear")
        global username

        filename = "./usr/" + username + '.dat'
        with open(filename, 'rb') as f:
                player0 = pickle.load(f)
                player1 = pickle.load(f)
                player2 = pickle.load(f)
                player3 = pickle.load(f)
                player4 = pickle.load(f)
                player5 = pickle.load(f)
                player6 = pickle.load(f)
                player7 = pickle.load(f)
                player8 = pickle.load(f)
                player9 = pickle.load(f)
	print("The tavern shop has some very basic gear for sale here")
	print("would you like to B)uy or S)ell?\n\n")
	tavern_select = raw_input("B)uy or S)ell ")
	if tavern_select == "b" or tavern_select == "B":
		shell.call("clear")
		print("What would you like to buy?")
		print("1) Dagger")
		print("2) Warhammer")
		print("3) Staff\n\n")
		buy_select = input("1 - 3 ")
		if buy_select == 1:
			temp_item = dagger()
			print("Who is purchasing this item? 1 - 6\n\n")
			player_select = input("Party member 1 - 6 ")
			player_select = player_select - 1
			player_check = vars()['player' + str(player_select)]
			if player_check.gp < temp_item.value:
				print(player_check.name + " does not have enough gold pieces")
				return
			else:
				if player_check.inventory.get("item0") == 'none':
					player_check.inventory["items0"] = dagger()
					save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
					print("Dagger added to " + player_check.name + "'s inventory")
					time.sleep(2)
				return
	elif tavern_select == "b" or tavern_select == "B":
		print("later")
	else:
		print("later")
	return



def disclaimer():
	shell.call("clear")
	print("This is a extremely alpha test of my text based rogue like based loosely on the Swords & Wizardry rule set.")
	print("Due to the extremely chaotic design process please expect that your save files if you choose to playtest")
	print("will be removed and most likely often, as features will change and rapidly. The game engine still does not")
	print("have enough implimented to be called a game and thus will retain its 1.0.0 status until I feel it is far")
	print("enough along that changing this will denote significant change to gameplay. With all that said have fun")
	print("-Reece")
	raw_input("press enter to continue")
	shell.call("clear")



def debug():
	global username

	filename = "./usr/" + username + '.dat'
	with open(filename, 'rb') as f:
		player0 = pickle.load(f)
		player1 = pickle.load(f) 
		player2 = pickle.load(f) 
		player3 = pickle.load(f) 
		player4 = pickle.load(f) 
		player5 = pickle.load(f) 
		player6 = pickle.load(f) 
		player7 = pickle.load(f)  
		player8 = pickle.load(f)  
		player9 = pickle.load(f)
		print(player0.name)
		print(player0._class)
		print(player0.race)
		print(player0.str)
		print(player0.dex)
		print(player0.con)
		print(player0.int)
		print(player0.wis)
		print(player0.cha)
		print(player0.xp)
		print(player0.xpbonus)
		print(player0.gp)
		print(player0.inventory.get("item0", "none"))#j1

		print(player1.name)
                print(player1._class)
                print(player1.race)
                print(player1.str)
                print(player1.dex)
                print(player1.con)
                print(player1.int)
                print(player1.wis)
                print(player1.cha)
                print(player1.xp)
                print(player1.xpbonus)
		print(player1.gp)

		print(player2.name)
        	print(player3.name)
        	print(player4.name)
        	print(player5.name)
        	print(player6.name)
        	print(player7.name)
        	print(player8.name)
       		print(player9.name)
		time.sleep(5)
		return		


def rolldice(numberofdice, maxvalue):
        rolledvalue = 0
        for i in range(0,numberofdice):
                rolledvalue += random.randint(1,maxvalue)
        return rolledvalue

def save(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9):
	global username
	player0 = p0
	player1 = p1
        player2 = p2
        player3 = p3
        player4 = p4
        player5 = p5
        player6 = p6
        player7 = p7
        player8 = p8
        player9 = p9

        filename = "./usr/" + username + '.dat'
        with open (filename, "wb") as f: #create hero save data
                pickle.dump(player0, f, protocol = 2)
                pickle.dump(player1, f, protocol = 2)
                pickle.dump(player2, f, protocol = 2)
                pickle.dump(player3, f, protocol = 2)
                pickle.dump(player4, f, protocol = 2)
                pickle.dump(player5, f, protocol = 2)
                pickle.dump(player6, f, protocol = 2)
                pickle.dump(player7, f, protocol = 2)
                pickle.dump(player8, f, protocol = 2)
                pickle.dump(player9, f, protocol = 2)
                return

def save_char(t1):
	temp_player = t1

	global username
	filename = "./usr/" + username + '.dat'
	with open(filename, 'rb') as f:
		player0 = pickle.load(f)
		player1 = pickle.load(f) 
		player2 = pickle.load(f) 
		player3 = pickle.load(f) 
		player4 = pickle.load(f) 
		player5 = pickle.load(f) 
		player6 = pickle.load(f) 
		player7 = pickle.load(f) 
		player8 = pickle.load(f) 
		player9 = pickle.load(f)

	if player0.name == "none":
		with open(filename, 'rb') as f:
			player0 = temp_player
			save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
			print(player0.name + " Saved")
			time.sleep(2)
	elif player1.name == "none":
                        player1 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
        		print(player1.name + " Saved")
			time.sleep(2)
	elif player2.name == "none":
                        player2 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
        		print(player2.name + " Saved")
			time.sleep(2)
	elif player3.name == "none":
                        player3 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
        		print(player3.name  + " Saved")
			time.sleep(2)
	elif player4.name == "none":
                        player4 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
        		print(player4.name  + " Saved")
			time.sleep(2)
	elif player5.name == "none":
                        player5 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
	        	print(player5.name  + " Saved")
			time.sleep(2)
	elif player6.name == "none":
                        player6 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
        		print(player6.name  + " Saved")
			time.sleep(2)
	elif player7.name == "none":
                        player7 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
        		print(player7.name  + " Saved")
			time.sleep(2)
	elif player8.name == "none":
                        player8 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
        		print(player8.name  + " Saved")
			time.sleep(2)
	elif player9.name == "none":
                        player9 = temp_player
                        save(player0, player1, player2, player3, player4, player5, player6, player7, player8, player9)
        		print(player9.name  + " Saved")
			time.sleep(2)

	else:
		print("There is no more rooms available at the tavern")
		time.sleep(2)
	return

def race_select():
	temp_player = player("none")
	shell.call("clear")
	print("Please select a race:")
	print("1) Human 2) Elf 3) Dwarf 4) Half-Elf 5) Hobbit 6) Help")
	while True:
        	race_choice = raw_input("Race Selection: ")
        	if race_choice == "1":
                	temp_player.race = "Human"
			return(temp_player.race)
        	elif race_choice == "2":
                	temp_player.race = "Elf"
			return(temp_player.race)
        	elif race_choice == "3":
                	temp_player.race = "Dwarf"
			return(temp_player.race)
        	elif race_choice == "4":
                	temp_player.race = "Half-Elf"
			return(temp_player.race)
        	elif race_choice == "5":
                	temp_player.race = "Hobbit"
			return(temp_player.race)
		elif race_choice == "6":
			races()
			shell.call("clear")
			print("1) Human 2) Elf 3) Dwarf 4) Half-Elf 5) Hobbit 6) Help")
        	else:
                	print("That is not a valid selection, try again")

def tavern():
	temp_player = player("none")
	shell.call("clear")
	print("Welcome to the Golden Boar Tavern and Inn, What can we do for you?")
	print("1) Find some traveling companions")
	print("2) Gather a traveling party")
	print("3) Purchase basic supplies")
	print("4) Listen to Bard")
	print("5) Highscores")
	print("6) Quit")

	while True:
		selection = raw_input("What would you like to do? ")
		if selection == "1":
			temp_player.str = rolldice(3,6)
			temp_player.dex = rolldice(3,6)
			temp_player.con = rolldice(3,6)
			temp_player.int = rolldice(3,6)
			temp_player.wis = rolldice(3,6)
			temp_player.cha = rolldice(3,6)
			print('STR: {}').format(temp_player.str)
                        print('DEX: {}').format(temp_player.dex)
                        print('CON: {}').format(temp_player.con)
                        print('INT: {}').format(temp_player.int)
                        print('WIS: {}').format(temp_player.wis)
                        print('CHA: {}').format(temp_player.cha)
			print('Please Select a class')
			print('F)ighter, C)leric, T)hief, M)age H)elp\n')
			while True:
				temp_player.xpbonus = 0
				class_select = raw_input('Class selection: ')
				shell.call("clear")
				if class_select == "h" or class_select == "H":
					professions()
					shell.call("clear")
					print('F)ighter, C)leric, T)hief, M)age H)elp\n')
				if class_select == "f" or class_select == "F":
					temp_player._class = 'Fighter'
					temp_player.hp = rolldice(1,8)
					temp_player.maxhp = temp_player.hp
					if temp_player.con >= 13:
						temp_player.hp = temp_player.hp + 1
						temp_player.maxhp = temp_player.hp
					if temp_player.con <= 8 and temp_player.hp >=2:
						temp_player.hp = temp_player.hp - 1
						temp_player.maxhp = temp_player.hp
					temp_player.save = 14
					temp_player.xp = 0
					temp_player.xpn = 2000
					if temp_player.str >= 13:
						temp_player.xpbonus = temp_player.xpbonus + 5
                                        if temp_player.wis >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        if temp_player.cha >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
					temp_player.race = race_select()
					if temp_player.race == "Human":
						temp_player.xpbonus = temp_player.xpbonus + 10
					if temp_player.race == "Half-Elf":
                                                temp_player.xpbonus = temp_player.xpbonus + 5
					shell.call('clear')
					temp_player.gp = (rolldice(3,6) * 10)
					print('Class: {}').format(temp_player._class)
					print('Race: {}').format(temp_player.race)
                                        print('HP: {}').format(temp_player.hp)
                                        print('Save: {}').format(temp_player.save)
                                        print('STR: {}').format(temp_player.str)
                                        print('DEX: {}').format(temp_player.dex)
                                        print('CON: {}').format(temp_player.con)
                                        print('INT: {}').format(temp_player.int)
                                        print('WIS: {}').format(temp_player.wis)
                                        print('CHA: {}').format(temp_player.cha)
                                        print('Bonus XP: {}').format(temp_player.xpbonus)
                                        print('\n\n\n\nSave this charachter?')
					while True:
						save_yn = raw_input('Y/N ')
						if save_yn == 'y' or save_yn == 'Y':
							print('Enter a name')
						 	temp_player.name = raw_input()
							save_char(temp_player)
							return
						elif save_yn == 'n' or save_yn == 'N':
							print("The rejected character heads back to the bar")
							time.sleep(2)
							return
						else:
							print("Y or N only please")

					
                        	elif class_select == "c" or class_select == "C":
                                	temp_player._class = "Cleric"
					temp_player.hp = rolldice(1,6)
					temp_player.save = 15
					if temp_player.con >= 13:
                                                temp_player.hp = temp_player.hp + 1
                                                temp_player.maxhp = temp_player.hp
                                        if temp_player.con <= 8 and temp_player.hp >=2:
                                                temp_player.hp = temp_player.hp - 1
                                                temp_player.maxhp = temp_player.hp
                                        temp_player.save = 14
                                        temp_player.xp = 0
                                        temp_player.xpn = 2000
                                        if temp_player.wis >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 10
                                        if temp_player.cha >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        temp_player.race = race_select()
                                        if temp_player.race == "Human":
                                                temp_player.xpbonus = temp_player.xpbonus + 10
                                        if temp_player.race == "Half-Elf":
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        shell.call('clear')
                                        temp_player.gp = (rolldice(3,6) * 10)
                                        print('Class: {}').format(temp_player._class)
                                        print('Race: {}').format(temp_player.race)
                                        print('HP: {}').format(temp_player.hp)
                                        print('Save: {}').format(temp_player.save)
                                        print('STR: {}').format(temp_player.str)
                                        print('DEX: {}').format(temp_player.dex)
                                        print('CON: {}').format(temp_player.con)
                                        print('INT: {}').format(temp_player.int)
                                        print('WIS: {}').format(temp_player.wis)
                                        print('CHA: {}').format(temp_player.cha)
                                        print('Bonus XP: {}').format(temp_player.xpbonus)
                                        print('\n\n\n\nSave this charachter?')
                                        while True:

                                                save_yn = raw_input('Y/N ')
                                                if save_yn == 'y' or save_yn == 'Y':
                                                        print('Enter a name')
                                                        temp_player.name = raw_input()
                                                        save_char(temp_player)
                                                        return
                                                elif save_yn == 'n' or save_yn == 'N':
                                                        print("The rejected character heads back to the bar")
                                                        time.sleep(2)
                                                        return
                                                else:
                                                        print("Y or N only please")				
                                	return
                        	elif class_select == "t" or class_select == "T":
                                	temp_player._class = "Thief"
					temp_player.hp = rolldice(1,4)
					temp_player.save = 15
					if temp_player.con >= 13:
                                                temp_player.hp = temp_player.hp + 1
                                                temp_player.maxhp = temp_player.hp
                                        if temp_player.con <= 8 and temp_player.hp >=2:
                                                temp_player.hp = temp_player.hp - 1
                                                temp_player.maxhp = temp_player.hp
                                        temp_player.save = 14
                                        temp_player.xp = 0
                                        temp_player.xpn = 1250
                                        if temp_player.dex >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        if temp_player.wis >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        if temp_player.cha >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        temp_player.race = race_select()
                                        if temp_player.race == "Human":
                                                temp_player.xpbonus = temp_player.xpbonus + 10
                                        if temp_player.race == "Half-Elf":
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        shell.call('clear')
                                        temp_player.gp = (rolldice(3,6) * 10)
                                        print('Class: {}').format(temp_player._class)
                                        print('Race: {}').format(temp_player.race)
                                        print('HP: {}').format(temp_player.hp)
                                        print('Save: {}').format(temp_player.save)
                                        print('STR: {}').format(temp_player.str)
                                        print('DEX: {}').format(temp_player.dex)
                                        print('CON: {}').format(temp_player.con)
                                        print('INT: {}').format(temp_player.int)
                                        print('WIS: {}').format(temp_player.wis)
                                        print('CHA: {}').format(temp_player.cha)
                                        print('Bonus XP: {}').format(temp_player.xpbonus)
                                        print('\n\n\n\nSave this charachter?')
                                        while True:
                                                save_yn = raw_input('Y/N ')
                                                if save_yn == 'y' or save_yn == 'Y':
                                                        print('Enter a name')
                                                        temp_player.name = raw_input()
                                                        save_char(temp_player)
                                                        return
                                                elif save_yn == 'n' or save_yn == 'N':
                                                        print("The rejected character heads back to the bar")
                                                        time.sleep(2)
                                                        return
                                                else:
                                                        print("Y or N only please")
                                	return
                        	elif class_select == "m" or class_select == "M":
                                	temp_player._class = "Mage"
					temp_player.hp = rolldice(1,4)
                                	temp_player.save = 15
					if temp_player.con >= 13:
                                                temp_player.hp = temp_player.hp + 1
                                                temp_player.maxhp = temp_player.hp
                                        if temp_player.con <= 8 and temp_player.hp >=2:
                                                temp_player.hp = temp_player.hp - 1
                                                temp_player.maxhp = temp_player.hp
                                        temp_player.save = 14
                                        temp_player.xp = 0
                                        temp_player.xpn = 2500
                                        if temp_player.int >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        if temp_player.wis >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        if temp_player.cha >= 13:
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        temp_player.race = race_select()
                                        if temp_player.race == "Human":
                                                temp_player.xpbonus = temp_player.xpbonus + 10
                                        if temp_player.race == "Half-Elf":
                                                temp_player.xpbonus = temp_player.xpbonus + 5
                                        shell.call('clear')
                                        temp_player.gp = (rolldice(3,6) * 10)
                                        print('Class: {}').format(temp_player._class)
                                        print('Race: {}').format(temp_player.race)
                                        print('HP: {}').format(temp_player.hp)
                                        print('Save: {}').format(temp_player.save)
                                        print('STR: {}').format(temp_player.str)
                                        print('DEX: {}').format(temp_player.dex)
                                        print('CON: {}').format(temp_player.con)
                                        print('INT: {}').format(temp_player.int)
                                        print('WIS: {}').format(temp_player.wis)
                                        print('CHA: {}').format(temp_player.cha)
                                        print('Bonus XP: {}').format(temp_player.xpbonus)
                                        print('\n\n\n\nSave this charachter?')
                                        while True:
                                                save_yn = raw_input('Y/N ')
                                                if save_yn == 'y' or save_yn == 'Y':
                                                        print('Enter a name')
                                                        temp_player.name = raw_input()
                                                        save_char(temp_player)
                                                        return
                                                elif save_yn == 'n' or save_yn == 'N':
                                                        print("The rejected character heads back to the bar")
                                                        time.sleep(2)
                                                        return
                                                else:
                                                        print("Y or N only please")
                                	return
				else:
					print("Invalid selection, please try again")
					time.sleep(2)

		elif selection == "2":
                        print("Not implimented yet")
                        return

		elif selection == "3":
			tavern_shop()
			return
		elif selection == "4":
                        print("Not implimented yet")
			return
		elif selection == "0":
			debug()
			return
		elif selection == "5":
                        print("Not implimented yet")
			return
		elif selection == "6":
			exit()

		else:
			print("please select from 1-6")
			time.sleep(2)
def login():
        global username
	test = 0 
        while True:
                username = raw_input("Please enter a username or quit: ")
		if username == "quit":
			login()
			return
		filename = "./usr/" + username + ".usr"
                if os.path.exists(filename):
			f = open(filename, 'r')
			testpass = f.readline().rstrip('\n') #read the first line,strip the end of line, put into variable
			while True:
				temppass = getpass.getpass("Please enter your password: ")
				if temppass == testpass:
					readme = f.readline().rstrip('\n')
					print(readme)
					f.close()
					return
				else:
					print("Passwords did not match, try again")
					test = test + 1
                        		if test == 3:
                                		exit()
                        		else:
						time.sleep(2)
                else:

			print("User does not exist")
			test = test + 1
			if test == 3:
				exit()
			else:
				time.sleep(2)
                        



def user_check(): #check to see if an account exists
	global username
	while True:
		username = raw_input("Please enter a username: ")
		filename = "./usr/" + username + ".usr"
		if os.path.exists(filename):
			print("User already exists")
			time.sleep(2)
		else:
			return
	

def write_user():
	global username
	global password
	player0 = player("none")
        player1 = player("none")
        player2 = player("none")
        player3 = player("none")
        player4 = player("none")
        player5 = player("none")
        player6 = player("none")
        player7 = player("none")
        player8 = player("none")
        player9 = player("none")

	filename = "./usr/" + username + ".usr"
	with open (filename, "wb") as f: #create a new file where % is the username, append .usr for extention, and write to it
		f.write(password + '\n')
	filename = "./usr/" + username + ".dat"
	with open (filename, "wb") as f: #create hero save data
		pickle.dump(player0, f, protocol = 2)
		pickle.dump(player1, f, protocol = 2) 
		pickle.dump(player2, f, protocol = 2) 
		pickle.dump(player3, f, protocol = 2) 
		pickle.dump(player4, f, protocol = 2) 
		pickle.dump(player5, f, protocol = 2) 
		pickle.dump(player6, f, protocol = 2) 
		pickle.dump(player7, f, protocol = 2) 
		pickle.dump(player8, f, protocol = 2) 
		pickle.dump(player9, f, protocol = 2)
		return


def new_user():                      #setup a new user and define all basic variables
	global username
	global password
	shell.call("clear")
	user_check()
	

	while True:
		password = getpass.getpass("Please enter a password: ")
		password_confirm = getpass.getpass("Please confirm password: ")
	
		if password == password_confirm:
			write_user()
			print("User " + username + " created")
			return
		
		else:
			print("Passwords do not match try again")
			time.sleep(2)
			shell.call("clear")

def main():
	while True:
		print("\n\n\nPlease Make a Selection")
		print("1) Create a user")
		print("2) Login")
		print("0) Quit")
		login_choice = raw_input("\nSelection: ")

		if login_choice == "1":
			new_user()
			return
		
		elif login_choice == "2":
			login()
			return
		elif login_choice == "0":
			exit()
		else: 
			print("Invalid choice, try again")
			time.sleep(2)
			shell.call("clear")
	

#start of the main program
disclaimer()
main()
while True:
	tavern()
