SWRPG is a Roguelike done in the style of an solo player Multi User Dungeon or Interactive Fiction 
game (all text based). The idea is to assemble a party of up to 6 adventurers to explore 20 randomly
generated levels each more difficult then the last to get to the bottom where the dragon lives to reclaim
your kingdoms treasury. Death is permanent, and so players are encouraged to use caution if they are to get 
strong enough to defeat the dragon. The rule set is based on Swords & Wizardry which are free and is a 
retro-clone of the original whitebox Dungeons & Dragons. 

SWRPG is ultimately intended to be ran in a server style environment where the players cannot directly
have access to their character files (to prevent save scumming/cheating) with highscores for bragging rights.